1. Creating a new git repository

    git init

2. Clone remote repository

    git clone <repo-url>

3. push local repository to remote

    git push <remote-name> <branch-name>

4. adding files

    git add <filename>

    git add .                   (to add all files)

5. discarding files (discard un-added changes)

    git checkout <filename>

6. commit changes

    git commit -m "<commit-message>"

7. create new branch (and switch to it)

    git checkout -b <branch-name>

8. switch to an existing branch

    git checkout <branch-name>

9. delete branches (local)

    git branch -D <branch-name>

10. delete branches (remote)

    git push --delete <remote-name> <branch-name>

11. push branch to remote repository

    git push <remote-name>  <branch-name>

12. merge changes from another branch

    git checkout <branch-to-merge-into>
    git merge <branch-to-merge-from>

13. view differences between 2 branches

    git diff <branch1> <branch2>

14. update local repository with remote changes

    git pull <remote-name>  <branch-name>

15. check commit logs

    git log

16. check commit status

    git status

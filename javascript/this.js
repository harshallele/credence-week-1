var obj = {
    a: 7,
    b: function() {
        return this.a;
    }
}

console.log(obj.b())

console.log(obj.b.apply({a:10}));
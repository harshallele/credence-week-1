let set = new Set();

set.add(4)
set.add(1);
set.add("lorem ipsum dotum");

set.add({a:10,b:20});

let obj = {                 // this object is distinct from the upper one, so it is added to the set
    a:10,                   // even though it has the same properties with the same values
    b:20
}

set.add(obj);

set.add(4);                 // doesnt do anything, because 4 is already in the set


console.log(set);

console.log(set.has(4));        //true

set.delete(obj);

console.log(set.has(obj));      // false, even though there is another object in the set with the same properties and values

//iterating over the set
set.forEach((val) => console.log(val));

console.log(set.size);

//clear all values in the set
set.clear()
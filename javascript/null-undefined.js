var n;

console.log(n == undefined)  //variables not initialised are undefined

console.log(typeof(a) == 'undefined')  // variables that have not been declared at all are of type undefined

console.log(n == true)    //undefined interpreted as false

var m = null

console.log(m == true)    // null variables considered false

console.log(m == undefined)   // true when comparing loosely

console.log(m === undefined) // false when comparing strictly





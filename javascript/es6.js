var x = 10;
let y = 100;
const z = 1000;


{
    var x;
    let y;
    const z = 2000;

    console.log(x);             //prints 10, because variables declared with "var" dont have block scope.
    console.log(y);             // prints "undefined", because variables declared with "let" have block scope.
    
    console.log(z);             // constant variables can't be changed in the scope they were defined in, 
                                // but they have block scope
}

//z = 3000;                       //causes error

//arrow function
const add = (a,b) => a+b;

console.log(add(5,2));

//classes
class Car{
    constructor(name,company){
        this.name = name;
        this.company = company;
    }

    printDetails(){
        console.log(this.name);
        console.log(this.company);
    }
}

let tiago = new Car("Tata","Tiago");
tiago.printDetails();


this.a = 1000;

//  in normal functions, the this object is specific to the scope of the function
//  so it prints undefined
function printA(){
    console.log("A: " + this.a);    
}

//arrow functions dont have their own this object. 
//so it prints the value of A correctly.
let _printA = () => console.log("A: " + this.a);

printA();
_printA();

// default arguments
function power(a,b=2){
    return (a ** b);
}

console.log(power(2));              // b not specified, but the default value is specified in function definition
console.log(power(2,3));


let arr = [20,200,354,401,5121,690];

// array.find() returns first element of array that passes the test function 
// (test function returns true when number is not divisible by 10)
let firstDivisbleBy10 = arr.find((val,index,arr) => {
    return ((val % 10) != 0);
})

console.log(firstDivisbleBy10);

// returns the index of the first element of array that passes the test function 
let indexDivisibleBy10 = arr.findIndex((val,index,arr) => {
    return ((val % 10) != 0);
}) 

console.log(indexDivisibleBy10);
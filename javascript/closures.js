let Car = (function(){
    let speed = 10;
    
    return {
        increaseSpeed: function(){
            speed += 10;
        },

        decreaseSpeed: function(){
            if(speed >= 10) speed -= 10;
        },

        getSpeed: function(){
            return speed;
        }
    }
})();

//speed variable is outside of the scope of returned object, but it can still be used because of closures
console.log(Car.getSpeed());    
Car.increaseSpeed();
console.log(Car.getSpeed());
Car.decreaseSpeed();
console.log(Car.getSpeed());

console.log(Car.speed);  //undefined

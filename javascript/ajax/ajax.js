let btn = document.getElementById("btn");

btn.addEventListener("click",btnClick);


function btnClick(event){
    
    document.getElementById("text").innerHTML = "";

    let city = document.getElementById("city-input").value;
    
    if(city != "" && city != null){

        var locationReq = new XMLHttpRequest();
        locationReq.open("GET","https://www.metaweather.com/api/location/search/?query=" + city,true);
        locationReq.onreadystatechange = function(){
            if (this.readyState == 4 && this.status == 200){
                let resp = JSON.parse(this.responseText);
                let woeid = resp[0].woeid;
                document.getElementById("text").innerHTML += "Weather for " + resp[0].title + "\n";
                getWeather(woeid);
            }
            else{
                document.getElementById("text").innerHTML == "Error: cant find location";
            }
        }
        locationReq.send();

    }

}


function getWeather(woeid){
    let weatherReq = new XMLHttpRequest();
    weatherReq.open("GET","https://www.metaweather.com/api/location/" + woeid + "/",true);

    weatherReq.onreadystatechange = function(){
        
        if (this.status == 200 && this.readyState == 4){
            let resp = JSON.parse(this.responseText).consolidated_weather[0];
            let weatherString = "<br>" + parseInt(resp.the_temp) + "C, " + resp.weather_state_name + "<br>";
            weatherString += "Max Temp: " + parseInt(resp.max_temp) + "C<br>";
            weatherString += "Min Temp: " + parseInt(resp.min_temp) + "C<br>";
            weatherString += "Wind: " + parseInt(resp.wind_speed) + "mph<br>";
            weatherString += "Humidity: " + resp.humidity + "%<br>";

            document.getElementById("text").innerHTML += weatherString;
        }

    }
    weatherReq.send();
}

let x = 10;
//not a pure function because it uses values from outside the function
//if the value of 10 was changed by some other function outside, this function would not change
function add10(val){
    return val + x;
}
//pure function because it doesnt use any outside variables or methods
function add10(val){
    return x + 10;
}

console.log(Math.min(1,2,3,4,));        // Math.min is a pure function because it returns
                                        // the same output for the same input everytime

console.log(Math.random());             //Math.random is not a pure function because it returns a different output everytime
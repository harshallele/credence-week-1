// for loop
for(var i = 0; i < 10; i++) {
    let str = ""
    for(var j = i; j > 0; j--){
        str+= "*";
    }
    console.log(str);
}

let arr = ["a","b","c","d","e"];

//logs index of elements in arr
for (i in arr){
    console.log(i);
}

//logs elements in arr
for (i of arr){
    console.log(i);
}

// applies function on each element of arr
arr.forEach((val,i,arr) => {
    console.log(val.toUpperCase());
});

//while loop
i = 0;
while(i < arr.length){
    let j = 0;
    let str = "";
    while(j <= i){
        str+=arr[j];
        j++;
    }
    console.log(str);
    i++;
}


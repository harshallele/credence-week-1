let sentence = "Lorem ipsum dolor sit amet, consectetur adipiscing elit,\
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\
 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

// converts first letter to uppercase
function wordTitleCase(str){
    let t = str[0].toUpperCase() + str.substr(1);
    return t;
}

function print(str){
    console.log(str);
}

// takes string, a transform function applied to every word in string, and a callback function.
//applyToWord is a higher-order function because it takes another function as its argument
function applyToWord(str,transform,callback){
    let words = str.split(" ");
    let newString = ""
    for(word of words){
        newString += transform(word) + " ";
    }

    callback(newString);
}

// used to convert a sentence to title case
applyToWord(sentence,wordTitleCase,print);
this.a = 10;

function A(){
    this.a = 20;
    console.log("inside A: " + this.a);
}

var f = () => {
    this.a = 30;
    console.log("inside f:" + this.a);
}

console.log("before calling a: " + this.a);
A();
console.log("after calling a, before f: " + this.a);
f();
console.log("after calling f: " + this.a);
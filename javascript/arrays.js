arr = [1,2,3,4,5,6,7,8,9,10];

console.log(arr.length);
console.log(arr[5]);

arr.push(12);
console.log(arr);
arr.pop();
console.log(arr);

arr.shift()
console.log(arr);
arr.unshift(0);
console.log(arr);

console.log(arr.indexOf(2));

arr.splice(8,1);  // remove element at position 8

console.log(arr);

arr.length = 5 // truncates arr to 5 elements

console.log(arr);
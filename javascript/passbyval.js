let x = 10;

function A(no){
    no = 20;
    console.log("Inside function: " + no);
}

console.log("Outside function, before calling it: " + x);
A(x);
console.log("Outside function, after calling it: " + x);


let a = {
    x: 10
};

function B(obj){
    obj.x = 20;
    console.log("Inside function: " + obj.x);
}

console.log("Outside function, before calling it: " + a.x);
B(a);
console.log("Outside function, after calling it: " + a.x);

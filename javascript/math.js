console.log(Math.PI);

console.log(Math.round(4.55));   // rounds up or down, to the nearest integer - prints 5

console.log(Math.floor(4.55));   // rounds down to the nearest integer  - prints 4

console.log(Math.ceil(4.55));    // rounds up to the nearest integer - print 5

console.log(Math.pow(2,5));      // raises 2 to the power of 5

console.log(Math.abs(-4.55));   //returns absolute value 

console.log(Math.min(2,3,5,10,12));     // returns minimum value

console.log(Math.max(2,3,5,10,12));     // returns max value

console.log(Math.sqrt(64));             // returns square root of 64


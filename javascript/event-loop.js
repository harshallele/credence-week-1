console.log("first message");

// the message is actually printed after the second message, because the callback function is not added to the function stack immediately
setTimeout(function(){
    console.log("this message should be logged after the first message and before the second message");
},0);

console.log("second message");

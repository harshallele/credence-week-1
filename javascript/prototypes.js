function Car(name,company,color,year){
    this.name = name;
    this.company = company;
    this.color = color;
    this.year = year;
}


var ferrari = new Car("F40","ferrari","red","2011");

//add printDetails method to Car object using prototype
Car.prototype.printDetails = function(){
    console.log("Name: " + this.name );
    console.log("Company: " + this.company);
    console.log("Color: " + this.color);
    console.log("Year: " + this.year);
}


console.log(ferrari.printDetails());

console.log(Object.getPrototypeOf(ferrari));    // returns the Car function


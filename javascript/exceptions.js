

try{
    let n = new Array(10000000000);         // range error:  array is too big
    
}catch(err){
    console.log(err);
}

let x = 10;

try {
    let y = x + z;                  // ReferenceError: z not defined
}catch(err){
    console.log(err)
}

try{
    if (x <= 10) throw new RangeError("x is less than or equal to 10")          //throwing your own errors
}catch(err){
    console.log(err);
}


try {
    //TypeError: x is not of the type it is supposed to be
    if((typeof(x) == "number")) throw new TypeError("x cant be of type number");        
}catch(err){
    console.log(err);
}
var Car = function() {

    return {
        model: "",
        company: "",
        color: "",
        year: 0,
        details: function(){
            console.log("Model: " + this.model);
            console.log("Company: " + this.company);
            console.log("Color: " + this.color);
            console.log("Year: " + this.year);
        },
        carName: function(){
            console.log(this.company + " " + this.model);
        }
    }
}


var HondaCivic = Car();
HondaCivic.model = "Civic";
HondaCivic.company = "Honda";
HondaCivic.color = "red";
HondaCivic.year = 2020;

console.log(HondaCivic.details());
console.log(HondaCivic.carName());
console.log(Object.keys(HondaCivic));
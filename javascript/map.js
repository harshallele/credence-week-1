let map = new Map();

let obj = {};

//anything can be set to key or value - strings, numbers, objects, etc
map.set("key1","some value");
map.set(2,20);
map.set(obj,1010);

console.log(map.has(2));            // true
console.log(map.get(obj));           // 1010

console.log(map.entries())          // prints entire map 

console.log(map.keys());            //prints the keys

//iterating over the map
map.forEach((val,key) => console.log(key + " -> " + val));

console.log(map.size)

//clears the map
map.clear();
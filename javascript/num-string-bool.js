console.log(Number.MAX_SAFE_INTEGER);   // most positive value:  2^53 - 1
console.log(Number.MIN_SAFE_INTEGER);   // most negative value: -2^53 - 1

console.log(Number.MAX_VALUE);          // highest absolute value a number can have
console.log(Number.MIN_VALUE);         // lowest absolute value a number can have

console.log(Number.POSITIVE_INFINITY);   // special value indicating positive infinity
console.log(Number.NEGATIVE_INFINITY);   // special value indicating negative infinity


let str1  = "this is a single line string"

let str2 = "this is a multi-line string \
using backslash to escape newlines \
When printed, it will print in a single line.";

console.log(str2);

console.log(str2.charAt(5));    // returns character at 5th position
console.log(str2.concat(str1));   // adds a to end of b
console.log(str2.toLowerCase());
console.log(str2.toUpperCase());

let a = 'A';
let b = 'B';

if (a < b) console.log('A less than B');        // string comparison using less/greater-than operators


if (1 == true) console.log("positive numbers interpreted as true");

if (-1 == true) console.log("negative numbers interpreted as true");
else console.log("negative numbers interpreted as false");

if (0 == true) console.log("zero interpreted as true");
else console.log("zero interpreted as false");

// non-positive numbers are considered false


